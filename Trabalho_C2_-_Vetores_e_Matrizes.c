#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <time.h>

// Função de Delay, peguei na net kkkkkk
void delay(unsigned int mseconds)
{
    clock_t goal = mseconds + clock();
    while (goal > clock());
}

int main (){
	setlocale(LC_ALL, "");
	
	char chrInicio;
	int intComando = 0, intComando2 = 0, vtrPresidente[15], vtrGovernador[12], intVotoUsuario, i, intMaisVotosPresidente, intMaisVotosGovernador, intPresidenteEleito, intGovernadorEleito;

	vtrPresidente[0] = 0, vtrPresidente[1] = 0, vtrPresidente[2] = 0, vtrPresidente[3] = 0, vtrPresidente[4] = 0, vtrPresidente[5] = 0, vtrPresidente[6] = 0, vtrPresidente[7] = 0, vtrPresidente[8] = 0, vtrPresidente[9] = 0, vtrPresidente[10] = 0, vtrPresidente[11] = 0, vtrPresidente[12] = 0, vtrPresidente[13] = 0, vtrPresidente[14] = 0, vtrPresidente[15] = 0, vtrGovernador[0] = 0, vtrGovernador[1] = 0, vtrGovernador[2] = 0, vtrGovernador[3] = 0, vtrGovernador[4] = 0, vtrGovernador[5] = 0, vtrGovernador[6] = 0, vtrGovernador[7] = 0, vtrGovernador[8] = 0, vtrGovernador[9] = 0, vtrGovernador[10] = 0, vtrGovernador[11] = 0, vtrGovernador[12] = 0, intMaisVotosPresidente = 0;

	printf("Digite 'S' para votar ou 'N' para sair. \n");
	scanf("%c", &chrInicio);
	fflush(stdin);
		while(intComando == 0){

			if (chrInicio == 'S' || chrInicio == 's')
			{
				intComando = 1;

					// Votação Presitente

					printf("Informe o número do seu candidato à presidência: \n");
					scanf("%d", &intVotoUsuario);
					switch(intVotoUsuario){
						case 0:
							printf("\nSeu voto foi computado como BRANCO.\n\n");
							vtrPresidente[0] = vtrPresidente[0] + 1;
							break;
						case 1:
							printf("\nSeu voto foi computado para Alvaro Dias (Podemos).\n\n");
							vtrPresidente[1] = vtrPresidente[1] + 1;
							break;
						case 2:
							printf("\nSeu voto foi computado para Cabo Daciolo (Patriota).\n\n");
							vtrPresidente[2] = vtrPresidente[2] + 1;
							break;
						case 3:
							printf("\nSeu voto foi computado para Ciro Gomes (PDT).\n\n");
							vtrPresidente[3] = vtrPresidente[3] + 1;
							break;
						case 4:
							printf("\nSeu voto foi computado para Eymael(DC).\n\n");
							vtrPresidente[4] = vtrPresidente[4] + 1;
							break;
						case 5:
							printf("\nSeu voto foi computado para Fernando Haddad (PT).\n\n");
							vtrPresidente[5] = vtrPresidente[5] + 1;
							break;
						case 6:
							printf("\nSeu voto foi computado para Geraldo Alckmin (PSDB).\n\n");
							vtrPresidente[6] = vtrPresidente[6] + 1;
							break;
						case 7:
							printf("\nSeu voto foi computado para Guilherme Boulos (PSol).\n\n");
							vtrPresidente[7] = vtrPresidente[7] + 1;
							break;
						case 8:
							printf("\nSeu voto foi computado para Henrique Meirelles (MDB).\n\n");
							vtrPresidente[8] = vtrPresidente[8] + 1;
							break;
						case 9:
							printf("\nSeu voto foi computado para Jair Bolsonaro (PSL).\n\n");
							vtrPresidente[9] = vtrPresidente[9] + 1;
							break;
						case 10:
							printf("\nSeu voto foi computado para João Amoêdo (Novo).\n\n");
							vtrPresidente[10] = vtrPresidente[10] + 1;
							break;
						case 11:
							printf("\nSeu voto foi computado para João Vicente Goulart (PPL).\n\n");
							vtrPresidente[11] = vtrPresidente[11] + 1;
							break;
						case 12:
							printf("\nSeu voto foi computado para Marina Silva (Rede).\n\n");
							vtrPresidente[12]= vtrPresidente[12] + 1;
							break;
						case 13:
							printf("\nSeu voto foi computado para Vera Lúcia (PSTU).\n\n");
							vtrPresidente[13] = vtrPresidente[13] + 1;
							break;
						case 14:
							printf("\nSeu voto foi computado como NULO.\n\n");
							vtrPresidente[14] = vtrPresidente[14] + 1;
							break;
						default:
							printf("\nCandidato Inválido!!\n\n");
						break;
					}

					// Votação Governador

						printf("Informe o número do seu candidato à governador: \n");
						scanf("%d", &intVotoUsuario);
						switch(intVotoUsuario){
							case 0:
								printf("\nSeu voto foi computado como BRANCO.\n\n");
								vtrGovernador[0] = vtrGovernador[0] + 1;
								break;
							case 1:
								printf("\nSeu voto foi computado para Cida Borghetti (Progressista).\n\n");
								vtrGovernador[1] = vtrGovernador[1] + 1;
								break;
							case 2:
								printf("\nSeu voto foi computado para Doutor Rosinha (PT).\n\n");
								vtrGovernador[2] = vtrGovernador[2] + 1;
								break;
							case 3:
								printf("\nSeu voto foi computado para Geonísio Marinho (PRTB).\n\n");
								vtrGovernador[3] = vtrGovernador[3] + 1;
								break;
							case 4:
								printf("\nSeu voto foi computado para João Arruda (MDB).\n\n");
								vtrGovernador[4] = vtrGovernador[4] + 1;
								break;
							case 5:
								printf("\nSeu voto foi computado para Jorge Bernardi (Rede).\n\n");
								vtrGovernador[5] = vtrGovernador[5] + 1;
								break;
							case 6:
								printf("\nSeu voto foi computado para Ogier Buchi (PSL).\n\n");
								vtrGovernador[6] = vtrGovernador[6] + 1;
								break;
							case 7:
								printf("\nSeu voto foi computado para Priscila Ebara (PCO).\n\n");
								vtrGovernador[7] = vtrGovernador[7] + 1;
								break;
							case 8:
								printf("\nSeu voto foi computado para Professor Ivan Bernardo (PSTU).\n\n");
								vtrGovernador[8] = vtrGovernador[8] + 1;
								break;
							case 9:
								printf("\nSeu voto foi computado para Professor Piva (PSOL).\n\n");
								vtrGovernador[9] = vtrGovernador[9] + 1;
								break;
							case 10:
								printf("\nSeu voto foi computado para Ratinho Junior (PSD).\n\n");
								vtrGovernador[10] = vtrGovernador[10] + 1;
								break;
							case 11:
								printf("\nSeu voto foi computado como NULO.\n\n");
								vtrGovernador[11] = vtrGovernador[11] + 1;
								break;
							default:
								printf("\nCandidato Inválido!!\n\n");
							break;
						}

			printf("###############################################################################\n");
			printf("#####                                                                     #####\n");
			printf("#####                                  FIM                                #####\n");
			printf("#####                                                                     #####\n");
			printf("###############################################################################\n\n");
			printf("                        AGUARDE O ENCERRAMENTO DA SESSÃO.                        ");
		
		
			delay(3000);
			system("cls");

			}else{
				if (chrInicio == 'N' || chrInicio == 'n')
				{
					system("cls");
					// Resultado Presidente
					printf("\n\nResultado da Apuração para Presidente:\n\n");
					printf("Alvaro Dias (Podemos): %d\n", vtrPresidente[1]);
					printf("Cabo Daciolo (Patriota): %d\n", vtrPresidente[2]);
					printf("Ciro Gomes (PDT): %d\n", vtrPresidente[3]);
					printf("Eymael(DC): %d\n", vtrPresidente[4]);
					printf("Fernando Haddad (PT): %d\n", vtrPresidente[5]);
					printf("Geraldo Alckmin (PSDB): %d\n", vtrPresidente[6]);
					printf("Guilherme Boulos (PSol): %d\n", vtrPresidente[7]);
					printf("Henrique Meirelles (MDB): %d\n", vtrPresidente[8]);
					printf("Jair Bolsonaro (PSL): %d\n", vtrPresidente[9]);
					printf("João Amoêdo (Novo): %d\n", vtrPresidente[10]);
					printf("João Vicente Goulart (PPL): %d\n", vtrPresidente[11]);
					printf("Marina Silva (Rede): %d\n", vtrPresidente[12]);
					printf("Vera Lúcia (PSTU): %d\n", vtrPresidente[13]);
					printf("Votos em BRANCO: %d\n", vtrPresidente[0]);
					printf("Votos em NULOS: %d\n", vtrPresidente[14]);
					// Resultado Governador
					printf("\n\nResultado da Apuração para Governador:\n\n");
					printf("Cida Borghetti (Progressista): %d\n", vtrGovernador[1]);
					printf("Doutor Rosinha (PT): %d\n", vtrGovernador[2]);
					printf("Geonísio Marinho (PRTB): %d\n", vtrGovernador[3]);
					printf("João Arruda (MDB): %d\n", vtrGovernador[4]);
					printf("Jorge Bernardi (Rede): %d\n", vtrGovernador[5]);
					printf("Ogier Buchi (PSL): %d\n", vtrGovernador[6]);
					printf("Priscila Ebara (PCO): %d\n", vtrGovernador[7]);
					printf("Professor Ivan Bernardo (PSTU): %d\n", vtrGovernador[8]);
					printf("Professor Piva (PSOL): %d\n", vtrGovernador[9]);
					printf("Ratinho Junior (PSD): %d\n", vtrGovernador[10]);
					printf("BRANCOS: %d\n", vtrGovernador[0]);
					printf("NULOS: %d\n", vtrGovernador[11]);

					// Apuração Votos Presidente
					for (i = 0; i < 15; ++i)
					{
						if (vtrPresidente[i] > intMaisVotosPresidente)
						{
							intMaisVotosPresidente = vtrPresidente[i];
							intPresidenteEleito = i;
						}
					}

					printf("\n\n\nO NOVO PRESIDENTE DO BRASIL É O(A): "); 

					switch(intPresidenteEleito){
						case 1:
						printf("Alvaro Dias (Podemos)\n");
						break;
						case 2:
						printf("Cabo Daciolo (Patriota)\n");
						break;
						case 3:
						printf("Ciro Gomes (PDT)\n");
						break;
						case 4:
						printf("Eymael(DC)\n");
						break;
						case 5:
						printf("Fernando Haddad (PT)\n");
						break;
						case 6:
						printf("Geraldo Alckmin (PSDB)\n");
						break;
						case 7:
						printf("Guilherme Boulos (PSol)\n");
						break;
						case 8:
						printf("Henrique Meirelles (MDB)\n");
						break;
						case 9:
						printf("Jair Bolsonaro (PSL)\n");
						break;
						case 10:
						printf("João Amoêdo (Novo)\n");
						break;
						case 11:
						printf("João Vicente Goulart (PPL)\n");
						break;
						case 12:
						printf("Marina Silva (Rede)\n");
						break;
						case 13:
						printf("Vera Lúcia (PSTU)\n");
						break;
					}

					// Apuração Votos Governador

					for (i = 0; i < 12; ++i)
					{
						if (vtrGovernador[i] > intMaisVotosGovernador)
						{
							intMaisVotosGovernador = vtrGovernador[i];
							intGovernadorEleito = i;
						}
					}

					printf("\n\nO NOVO GOVERNADOR DO ESTADO DO PARANÁ É O(A): "); 

					switch(intGovernadorEleito){
						case 1:
						printf("Cida Borghetti (Progressista)\n");
						break;
						case 2:
						printf("Doutor Rosinha (PT)\n");
						break;
						case 3:
						printf("Geonísio Marinho (PRTB)\n");
						break;
						case 4:
						printf("João Arruda (MDB)\n");
						break;
						case 5:
						printf("Jorge Bernardi (Rede)\n");
						break;
						case 6:
						printf("Ogier Buchi (PSL)\n");
						break;
						case 7:
						printf("Priscila Ebara (PCO)\n");
						break;
						case 8:
						printf("Professor Ivan Bernardo (PSTU)\n");
						break;
						case 9:
						printf("Professor Piva (PSOL)\n");
						break;
						case 10:
						printf("Ratinho Junior (PSD)\n");
						break;
					}

					exit (0);
				}else{
					printf("\n\nOpção inválida! Tente novamente.\n\n");
				}
			}
			intComando = 0;

			printf("\nDigite 'S' para votar ou 'N' para sair.\n");
			fflush(stdin);
			scanf("%c", &chrInicio);
		}

	system("pause");
	return 0;

}